package ass01.ex1.src;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Class for computing an image of a region
 * of the Mandelbrot set.
 * Concurrent version.
 *
 * @author Michele Sapignoli
 *
 */
public class MandelbrotSetImageConcurrentImpl implements MandelbrotSetImage {

	private int w,h;
	private int image[];
	private Complex center;
	private double delta;


	/**
	 * Creating an empty Mandelbrot set image.
	 *
	 * @param w width in pixels
	 * @param h height in pixels
	 * @param c center of the Mandelbrot set to be represented
	 * @param radius radius of the Mandelbrot set to be represented
	 */
	public MandelbrotSetImageConcurrentImpl(int w, int h, Complex c, double radius){
		this.w = w;
		this.h = h;
        image = new int[w*h];
        center = c;
        delta = radius/(w*0.5);
	}

	@Override
	public void compute(int nIterMax) {
		int xBegin = 0;

		int cores = Runtime.getRuntime().availableProcessors() + 1;

		// An Executor that provides methods to manage termination
		// and methods that can produce a Future for tracking progress of one or more asynchronous tasks.
		ExecutorService pool = Executors.newFixedThreadPool(cores);

		//Work divided in tasks > threads (number of cores)
		int tasksNumber = cores + 30;

		int xIncrement = w / tasksNumber;
		int i;
		for (i = 0; i < tasksNumber + 1; i++){
			pool.execute(new MandelbrotSubsetConcurrentComputer(this, xBegin, xBegin+xIncrement, nIterMax, i));
			xBegin += xIncrement;
		}

		pool.shutdown();
		try {
			pool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Compute the image with the specified level of detail
	 * 
	 * See https://en.wikipedia.org/wiki/Mandelbrot_set
	 * 
	 * @param nIterMax number of iteration representing the level of detail
	 */
	public void compute(int nIterMax, int xBegin, int xEnd){
		
		/*
		 * for each pixel of the image
		 * - get the corresponding point on the complex plan
		 * - verify if the point either belongs or not to the Mandelbrot set
		 * -- yes => black color 
		 * -- no => level of gray, depending on the distance from the set
		 */

		for (int x = xBegin; x < xEnd; x++ ){
			for (int y = 0; y < h; y++){
				Complex c = getPoint(x,y);
				double level = computeColor(c,nIterMax);
				int color = (int)(level*255);
				image[y*w+x] = color + (color << 8)+ (color << 16);
			}
		}
	}
		
	/**
	 * Basic Mandelbrot set algorithm
	 *  
	 * @param c
	 * @param maxIteration
	 * @return
	 */
	private double computeColor(Complex c, int maxIteration){
		int iteration = 0;		
		Complex z = new Complex(0,0);

		/*
		 * Repeatedly compute z := z^2 + c
		 * until either the point is out 
		 * of the 2-radius circle or the number
		 * of iteration achieved the max value
		 * 
		 */
		while ( z.absFast() <= 2 &&  iteration < maxIteration ){
			z = z.times(z).plus(c); 
			iteration++;
		  }		 
		  if ( iteration == maxIteration ){			  
			  /* the point belongs to the set */
			  return 0;
		  } else {
			  /* the point does not belong to the set => distance */
			  return 1.0-((double)iteration)/maxIteration;
		  }
	}
	
	/**
	 * This method returns the point in the complex plane
	 * corresponding to the specified element/pixel in the image
	 * 
	 * @param x x coordinate in the image
	 * @param y y coordinate in the image
	 * @return the corresponding complex point
	 */
	public Complex getPoint(int x, int y){
		return new Complex((x - w*0.5)*delta + center.re(), center.im() - (y - h*0.5)*delta); 
	}
	
	/**
	 * Get the height of the image
	 * @return
	 */
	public int getHeight(){
		return h;
	}

	/**
	 * Get the width of the image
	 * @return
	 */
	public int getWidth(){
		return w;
	}
	
	/**
	 * Get the image as an array of int, organized per rows
	 * (compatible with the BufferedImage style)
	 * 
	 * @return
	 */
	public int[] getImage(){
		return image;
	}

	public void updateRadius(double rad){
		delta = rad/(w*0.5);
	}

}

