package ass01.ex2.src;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * MainView: Swing class that extends JFrame
 * cointaining buttons and a Referencer
 */
public class MainView extends JFrame implements ActionListener {

	private static final long serialVersionUID = 783780900301298247L;
	
	private JButton startButton;
	private JButton stopButton;
	private JLabel countLabel;
	private Controller controller;
	
	public MainView() {
		this.startButton = new JButton("Start");
		this.stopButton = new JButton("Stop");
		this.countLabel = new JLabel("");
		this.setBounds(100, 100, 400, 100);
		this.setLayout(new FlowLayout());
		this.getContentPane().add(startButton);
		this.getContentPane().add(stopButton);
		this.getContentPane().add(countLabel);
		this.startButton.addActionListener(this);
		this.stopButton.addActionListener(this);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void setObserver(Controller observer) {
		this.controller = observer;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object button = e.getSource();
		if (button == startButton) {
			controller.startButtonPressed();
		} else if (button == stopButton) {
			controller.stopButtonPressed();
		}
	}	
	
	public void setLabelValue(int value) {
		SwingUtilities.invokeLater(() -> this.countLabel.setText("" + value));		
	}

}
