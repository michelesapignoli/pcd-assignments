package ass01.ex2.src;

public class CounterEvent {
	
	private int value;
	
	public CounterEvent(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
}
