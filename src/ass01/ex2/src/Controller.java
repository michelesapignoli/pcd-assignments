package ass01.ex2.src;

import ass01.ex2.src.threads.Pinger;
import ass01.ex2.src.threads.Ponger;
import ass01.ex2.src.threads.Viewer;

/**
 * Referencer class:
 * Methods startButtonPressed, stopButtonpressed and setView
 */
public class Controller {
	
	private MainView mainView;
	private SemaphoreHandler context;
	private Counter counter;
	private Pinger pinger;
	private Ponger ponger;
	private Viewer viewer;
	
	public Controller() {
		this.context = new SemaphoreHandler();
		this.counter = new Counter(0);
	}
	

	public void startButtonPressed() {
		this.pinger = new Pinger(counter, context);
		this.ponger = new Ponger(counter, context);
		this.viewer = new Viewer(mainView);
		this.counter.addListener(viewer);
		this.viewer.start();
		this.pinger.start();
		this.ponger.start();
	}

	public void stopButtonPressed() {
		this.pinger.setEnd();
		this.ponger.setEnd();
		this.viewer.setEnd();
		System.exit(0);
	}
	
	public void setView(MainView view) {
		this.mainView = view;
	}
	
	

}
