package ass01.ex2.src;

public class Main {
	
	public static void main(String[] args) {
		MainView m = new MainView();
		Controller c = new Controller();
		c.setView(m);
		m.setObserver(c);
		m.setVisible(true);
	}

}
