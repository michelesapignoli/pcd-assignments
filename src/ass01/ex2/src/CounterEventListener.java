package ass01.ex2.src;

public interface CounterEventListener {
	void counterIncrement(CounterEvent ev);
}
