package ass01.ex2.src.threads;

import ass01.ex2.src.SemaphoreHandler;
import ass01.ex2.src.Counter;

public class Pinger extends Thread {
	
	private Counter counter;
	private SemaphoreHandler context;
	private volatile boolean end;
	
	public Pinger(Counter counter, SemaphoreHandler context) {
		this.counter = counter;
		this.context = context;
		this.end = false;
	}
	
	@Override
	public void run() {
		while(!end) {
			counter.inc();
			System.out.println("ping!");
			context.getS2().release();
			try {
				context.getS1().acquire();
				if (end) { 
					break;
				}
			} catch (InterruptedException e) {}
		}	
		System.out.println("Pinger: \"Goodbye\"");
	}
	
	public void setEnd() {
		this.end = true;
		context.getS1().release();
	}

}
