package ass01.ex2.src.threads;

import ass01.ex2.src.SemaphoreHandler;
import ass01.ex2.src.Counter;

public class Ponger extends Thread {
	
	private Counter counter;
	private SemaphoreHandler context;
	private volatile boolean end;
	
	public Ponger(Counter counter, SemaphoreHandler context) {
		this.counter = counter;
		this.context = context;
		this.end = false;
	}
	
	@Override
	public void run() {
		while(!end) {
			try {
				context.getS2().acquire();
				if (end) { 
					break; 
				}
			} catch (InterruptedException e) {}
			counter.inc();
			System.out.println("pong!");
			context.getS1().release();
		}	
		System.out.println("Ponger: \"Goodbye\"");
	}
	
	public void setEnd() {
		this.end = true;
		context.getS2().release();
	}

}
