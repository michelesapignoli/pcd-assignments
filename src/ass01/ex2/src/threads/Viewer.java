package ass01.ex2.src.threads;

import ass01.ex2.src.CounterEvent;
import ass01.ex2.src.CounterEventListener;
import ass01.ex2.src.MainView;

public class Viewer extends Thread implements CounterEventListener {
	
	private volatile boolean end;
	private MainView mainView;
	
	public Viewer(MainView mainView) {
		this.mainView = mainView;
		this.end = false;
	}
	
	@Override
	public void run() {
		while(!end){}
		System.out.println("Viewer: \"Goodbye\"");
	}

	@Override
	public void counterIncrement(CounterEvent ev) {
		int value = ev.getValue();
		mainView.setLabelValue(value);
	}
	
	public void setEnd() {
		this.end = true;
	}

}
