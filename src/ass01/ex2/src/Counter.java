package ass01.ex2.src;

import java.util.ArrayList;

public class Counter {
	
	private int value;
	private ArrayList<CounterEventListener> listeners;
	
	public Counter(int startValue) {
		this.value = startValue;
		this.listeners = new ArrayList<>();
	}

	public void inc() {
		this.value++;
		notifyEvent(new CounterEvent(value));
	}
	
	@Override
	public String toString() {
		return "" + value;
	}
	
	public void addListener(CounterEventListener ls) {
		this.listeners.add(ls);
	}
	
	private void notifyEvent(CounterEvent ev){
	    for (CounterEventListener l: listeners){
	       l.counterIncrement(ev);
	    }
	}

}
