package ass01.ex2.src;

import java.util.concurrent.Semaphore;

/**
 * SemaphoreHandler class, cointaining the semaphores
 */
public class SemaphoreHandler {
	
	private Semaphore s1;
	private Semaphore s2;
	
	public SemaphoreHandler() {
		this.s1 = new Semaphore(0);
		this.s2 = new Semaphore(0);
	}
	
	public Semaphore getS1() {
		return s1;
	}
	
	public Semaphore getS2() {
		return s2;
	}

}
