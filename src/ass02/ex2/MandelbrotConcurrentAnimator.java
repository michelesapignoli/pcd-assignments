package ass02.ex2;

/**
 * MandelbrotConcurrentAnimator
 * with animation
 *
 */
public class MandelbrotConcurrentAnimator {
	public static void main(String[] args) throws Exception {
		
		/* size of the mandelbrot set in pixel */
		int width = 400;
		int height = 400;
		
		/* number of iteration */
		int nIter = 500;

		int nFrames = 0;

		/* region to be represented: center and radius */
		double rad0 = 2;
		Complex c1 = new Complex(-0.75,0.1);
		Complex c2 = new Complex(-0.1011,0.9563);
		Complex c3 = new Complex(0.254,0);
		Complex c4 = new Complex(0.001643721971153, 0.822467633298876);
		double radius = rad0;

		long totalTime = 0;

		MandelbrotSetImage set = new MandelbrotSetImageConcurrentImpl(width,height, c4, rad0);
		MandelbrotView view = new MandelbrotView(set, 410, 480);
		/* showing the image */
		view.setVisible(true);
		System.out.println("Started time");
		while(true) {
			while(view.isRunning()){
				System.out.println("Computing...");
				StopWatch cron = new StopWatch();
				cron.start();

				/* computing the image */
				set.compute(nIter);
				cron.stop();
				totalTime += cron.getTime();
				System.out.println("Frame " + nFrames + " done in " + cron.getTime());

				set.updateRadius(radius);
				radius *= 0.9;
				view.repaint();
				nFrames++;

			}
			System.out.println("pause - time elapsed: "+ totalTime + " for " + nFrames +" frames");


		}

	}


}
