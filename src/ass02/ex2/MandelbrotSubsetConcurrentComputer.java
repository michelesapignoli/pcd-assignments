package ass02.ex2;

public class MandelbrotSubsetConcurrentComputer implements Runnable {

	private MandelbrotSetImageConcurrentImpl set;
	private int xBegin;
	private int xEnd;
	private int nIterMax;
	private int id;

	public MandelbrotSubsetConcurrentComputer(MandelbrotSetImageConcurrentImpl set, int xBegin, int xEnd, int nIterMax, int id) {
		this.set = set;
		this.xBegin = xBegin;
		this.xEnd = xEnd;
		this.nIterMax = nIterMax;
		this.id=id;
	}

	public void run() {
		try {
			log("Task " +this.id+" started");
			set.compute(nIterMax, xBegin, xEnd);
			log("Task " +this.id+" done");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	
	private void log(String msg){
		synchronized(System.out){
			System.out.println(msg);
		}
	}

}
