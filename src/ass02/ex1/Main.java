package ass02.ex1;

import ass02.ex1.monitor.Oracle;
import ass02.ex1.threads.Player;


/**
 * @author Michele Sapignoli
 */
public class Main {
    private static int nPlayers = 9;

    public static void main(String[] args) {
        Oracle oracle = new Oracle(nPlayers);

        for(int i=0; i < nPlayers; i++){
            new Player(i,oracle).start();

        }
    }
}
