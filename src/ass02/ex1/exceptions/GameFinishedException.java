package ass02.ex1.exceptions;

public class GameFinishedException extends Exception {

    private String message;

    public GameFinishedException(boolean won, int playerId){
        if(won){
            this.message = "Won! | " + playerId;
        } else {
            this.message = "sob :( | " + playerId;
        }
    }

    @Override
    public String getMessage(){
        return this.message;
    }
}
