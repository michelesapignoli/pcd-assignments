package ass02.ex1.monitor;

import ass02.ex1.exceptions.GameFinishedException;
import ass02.ex1.model.Result;
import ass02.ex1.model.ResultImpl;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 *  @author Michele Sapignoli
 */
public class Oracle implements OracleInterface {
    private long oracleNumber;
    private LinkedList<Result> queries;
    private volatile boolean gameFinished;
    private int nPlayers;

    public Oracle(int nPlayers){
        this.oracleNumber = Math.abs(ThreadLocalRandom.current().nextLong());
        this.queries = new LinkedList<>();
        this.nPlayers = nPlayers;
    }

    @Override
    public boolean isGameFinished() {
        return this.gameFinished;
    }

    /**
     * Synchronized method that handles the players queries.
     * It adds the player query to a queue and sets the player awaiting until all the players arrive.
     * Then the methods unlocks all of the players but only the first "booked" can be served with his answer.
     *
     * @param playerId playerId
     * @param queryNumber queryNumber
     * @return result
     * @throws GameFinishedException end of the game
     */
    @Override
    public synchronized Result tryToGuess(int playerId, long queryNumber) throws GameFinishedException{

        Result result = new ResultImpl(playerId, queryNumber, this.oracleNumber);
        this.queries.add(result);

        // Waking up all threads when the queue is full
        if(queries.size() == this.nPlayers){
            notifyAll();
        }

        // Only the first "booked" thread can go ahead, the others wait
        while((queries.get(0).getPlayerId() != playerId || queries.size() < this.nPlayers)){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(this.gameFinished){
                throw new GameFinishedException(false, playerId);
            }
        }

        this.printResult(result);
        this.queries.removeFirst();

        return result;
    }


    private void printResult(Result guess) throws GameFinishedException{
        if(!gameFinished){
            System.out.println("Player " + guess.getPlayerId() + " asks for " + guess.getGuess());

            if(guess.found()){
                this.gameFinished = true;
                notifyAll();
                throw new GameFinishedException(true, guess.getPlayerId());
            }
        }
    }

}
