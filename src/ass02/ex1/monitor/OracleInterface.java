package ass02.ex1.monitor;

import ass02.ex1.exceptions.GameFinishedException;
import ass02.ex1.model.Result;

public interface OracleInterface {

	boolean isGameFinished();
	
	Result tryToGuess(int playerId, long value) throws GameFinishedException;
	
}
