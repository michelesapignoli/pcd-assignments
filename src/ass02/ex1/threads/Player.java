package ass02.ex1.threads;


import ass02.ex1.exceptions.GameFinishedException;
import ass02.ex1.monitor.Oracle;
import ass02.ex1.model.Result;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Michele Sapignoli
 */
public class Player extends Thread {
    private int id;
    private Oracle oracle;
    private long queryNumber;
    private long min, max;

    public Player(int id, Oracle monitor){
        this.id = id;
        this.oracle = monitor;
        this.min = 0;
        this.max = Long.MAX_VALUE;
        this.queryNumber = Math.abs(ThreadLocalRandom.current().nextLong());
    }


    @Override
    public void run() {
        Result answer;
        do try {

            answer = this.oracle.tryToGuess(this.id, this.queryNumber);
            this.computeQuery(answer);

        } catch (GameFinishedException ex) {
            System.out.println(ex.getMessage());
        } while(!this.oracle.isGameFinished());

    }

    private void computeQuery(Result answer){

        if (answer.isGreater()) {
            this.min = answer.getGuess();
        } else if (answer.isLess()) {
            this.max = answer.getGuess();
        }
        this.queryNumber = Math.abs(ThreadLocalRandom.current().nextLong(this.min, this.max));
    }

}
