package ass02.ex1.model;

public interface Result {
	
	boolean found();
	boolean isGreater();	
	boolean isLess();
	int getPlayerId();
	long getGuess();

}
