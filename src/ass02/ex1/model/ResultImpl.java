package ass02.ex1.model;

/**
 * @author Michele Sapignoli
 */
public class ResultImpl implements Result {
    private long guess;
    private int playerId;
    private long oracleNumber;

    public ResultImpl(int player, long guess, long oracleNumber){
        this.guess = guess;
        this.playerId = player;
        this.oracleNumber = oracleNumber;
    }

    @Override
    public int getPlayerId(){
        return this.playerId;
    }

    @Override
    public long getGuess() {
        return this.guess;
    }

    @Override
    public boolean found() {
        return this.guess == this.oracleNumber;
    }

    @Override
    public boolean isGreater() {
        return this.guess < this.oracleNumber;
    }

    @Override
    public boolean isLess() {
        return this.guess > this.oracleNumber;
    }
}
