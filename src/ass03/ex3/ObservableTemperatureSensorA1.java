package ass03.ex3;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import pcd.ass03.ObservableTemperatureSensor;
import pcd.ass03.acme.TemperatureSensorA1;

/**
 * ObservableTemperatureSensorA1 creates a stream from TemperatureSensorA1.
 * 
 * @author Michele Sapignoli
 *
 */
public class ObservableTemperatureSensorA1 extends TemperatureSensorA1 implements ObservableTemperatureSensor {
	
	public ObservableTemperatureSensorA1() {
		super();
		
	}
	
	public Flowable<Double> createObservable() {
		return Flowable.create(new FlowableOnSubscribe<Double>() {
			@Override
			public void subscribe(FlowableEmitter<Double> subscriber) throws Exception {
				new Thread(() -> {
					while (true) {
						subscriber.onNext(getCurrentValue());
						try {
							Thread.sleep(Constants.SLEEP_MS);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}).start();
			}
		}, BackpressureStrategy.BUFFER);	
	}


}