package ass03.ex3;

import java.text.DecimalFormat;
/**
 * Constants class.
 * @author Michele Sapignoli
 *
 */
public class Constants {
	public static final double SPIKE_H = 1000.0;
	public static final double SPIKE_L = -1000.0;
	public static final double THRESHOLD = 20.0;
	public static final DecimalFormat DF = new DecimalFormat("0.00");
	public static final int SLEEP_MS = 250;
    
}
