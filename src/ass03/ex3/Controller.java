package ass03.ex3;

import java.util.HashMap;
import java.util.Map;
import javax.swing.JLabel;
import io.reactivex.Flowable;
import pcd.ass03.ObservableTemperatureSensor;

/**
 * Controller class.
 * 
 * @author Michele Sapignoli
 *
 */
public class Controller {
	private Map<ObservableTemperatureSensor, Flowable<Double>> sensors;
	private View v;
	private boolean exceeded;

	public Controller(View v, Map map) {
		this.sensors = map;
		this.v = v;
	}

	/**
	 * startSensing creates a stream for the sensor given and notifies its label of the view 
	 * whenever a temperature is detected.
	 * 
	 * @param sensor
	 * @param label
	 */
	public void startSensing(ObservableTemperatureSensor sensor, JLabel label){
		this.sensors.put(sensor, sensor.createObservable());
		this.sensors.get(sensor).filter(degrees -> degrees < Constants.SPIKE_H && degrees > Constants.SPIKE_L).subscribe((x) -> {
			v.notifyTemperature(x, label);
		});
	}

	/**
	 * startSensingSum takes two streams in input and creates a new one with combineLatest:
	 * in this way the latest value detected by one of the two sensors is paired with the last of the other.
	 * The subscribe notifies the view with a value telling if the threshold is exceeded.
	 * 
	 * @param stream1
	 * @param stream2
	 */
	public void startSensingSum(ObservableTemperatureSensor sensor1, ObservableTemperatureSensor sensor2){
		try{		
			Flowable.combineLatest(
					this.sensors.get(sensor1).filter(x -> x> Constants.SPIKE_L && x < Constants.SPIKE_H), 
					this.sensors.get(sensor2).filter(x-> x> Constants.SPIKE_L && x < Constants.SPIKE_H), 
					(temp1, temp2) -> this.sumTemperatures(temp1, temp2)).subscribe((tot) -> {			
						this.v.notifyAlert(exceeded);					
					});
		} catch (Exception e){
			e.printStackTrace();
			
		}
	}
	
	private double sumTemperatures(double temp1, double temp2){
		this.exceeded = temp1 > Constants.THRESHOLD? (temp2 > Constants.THRESHOLD? true : false) : false;
		return temp1 + temp2;
	}

	public Flowable<Double> getFlowableFromSensor(ObservableTemperatureSensor sensor){
		return this.sensors.get(sensor);		
	}

	public Map<ObservableTemperatureSensor, Flowable<Double>> getSensors(){
		return this.sensors;
	}

}
