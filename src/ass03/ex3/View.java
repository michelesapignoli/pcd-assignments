package ass03.ex3;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import io.reactivex.Flowable;
import pcd.ass03.ObservableTemperatureSensor;
import pcd.ass03.acme.TemperatureSensorB1;
import pcd.ass03.acme.TemperatureSensorB2;

/**
 * View class, extending JFrame.
 * 
 * @author Michele Sapignoli
 *
 */
public class View extends JFrame{
	private Controller controller;
	private JLabel alertLabel;
		
	public View(){
		
		this.setSize(150, 400);
		this.controller = new Controller(this, new HashMap<ObservableTemperatureSensor, Flowable<Double>>());
		
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
	    
	    ImageIcon image1 = new ImageIcon("src/ass03/ex3/res/home1.png");
	    ImageIcon image2 = new ImageIcon("src/ass03/ex3/res/home2.png");
	    ImageIcon image3 = new ImageIcon("src/ass03/ex3/res/home3.png");
	    
	    JLabel l1 = new JLabel("");
	    l1.setIcon(image1);
	    l1.setPreferredSize(new Dimension(150,90));
	    JLabel l2 = new JLabel("");
	    l2.setIcon(image2);
	    l2.setPreferredSize(new Dimension(150,90));
	    JLabel l3 = new JLabel("");
	    l3.setIcon(image3);
	    l3.setPreferredSize(new Dimension(150,90));
	    this.alertLabel = new JLabel("Attention: Room2 & Room3 alert");
	    this.alertLabel.setForeground(Color.RED);
	    this.alertLabel.setVisible(false);
	    
	    panel.add(l1);
	    panel.add(l2);
	    panel.add(l3);
	    panel.add(this.alertLabel);
	    
	    TemperatureSensorB1 b1 = new TemperatureSensorB1();
	    TemperatureSensorB2 b2 = new TemperatureSensorB2();

	    /*
	     * Start sensing
	     */
	    this.controller.startSensing(new ObservableTemperatureSensorA1(), l1);
		this.controller.startSensing(b1, l2);
		this.controller.startSensing(b2, l3);
		
		this.controller.startSensingSum(b1, b2);
		
		this.add(panel);
		this.setVisible(true);
	}
	
	public void notifyTemperature(double x, JLabel l){
		SwingUtilities.invokeLater(new Runnable(){

			@Override
			public void run() {
				l.setText(Constants.DF.format(x) +"�C");
			}			
		});		
	}
	
	
	public void notifyAlert(boolean hasExceeded){
		SwingUtilities.invokeLater(new Runnable(){

			@Override
			public void run() {
					alertLabel.setVisible(hasExceeded);				
			}			
		});		
	}
	
	
}
