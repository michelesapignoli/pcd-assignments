package ass03.ex1.view;

import akka.actor.*;
import ass03.ex1.actors.Oracle;
import ass03.ex1.messages.GameStartedMsg;
import ass03.ex1.messages.PauseMsg;
import ass03.ex1.utils.Observer;
import ass03.ex1.utils.ObserverType;
import ass03.ex1.utils.Strings;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * View class.
 *
 * @author Michele Sapignoli
 */
public class View extends Observer {
    private ActorSystem context;
    private int HEIGHT = 150;
    private int WIDTH = 700;
    private boolean isRunning;
    private boolean pausing;

    private JButton startButton;
    private JButton stopButton;
    private JLabel numberLabel;
    private JLabel turnLabel;
    private JLabel turn;
    private JLabel oracleNumber;
    private JLabel winnerLabel;

    public View() {
        this.context = ActorSystem.create(Strings.SYSTEM_NAME);
        this.isRunning = false;
        this.pausing = false;
        this.createFrame();
    }

    private void createFrame() {
        this.setSize(this.WIDTH, this.HEIGHT);
        JPanel panel = new JPanel();
        this.startButton = new JButton("New Game");
        this.stopButton = new JButton("Stop");
        panel.add(this.startButton);
        panel.add(this.stopButton);

        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.NORTH);

        this.numberLabel = new JLabel("Oracle number: ");
        this.turnLabel = new JLabel("Turn: ");
        this.oracleNumber = new JLabel("?");
        this.turn = new JLabel("?");
        this.winnerLabel = new JLabel("");
        this.winnerLabel.setForeground(Color.red);

        panel.add(numberLabel);
        panel.add(oracleNumber);
        panel.add(turnLabel);
        panel.add(turn);
        panel.add(winnerLabel);

        final Observer view = this;

        this.startButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if(!isRunning){
                        isRunning = true;
                        ActorRef oracle = context.actorOf(Props.create(Oracle.class), Strings.ORACLE_NAME);
                        oracle.tell(new GameStartedMsg(Strings.PLAYERS, view), ActorRef.noSender());
                } else{
                    pausing = !pausing;
                    context.actorSelection(Strings.TD_ACTOR).tell(new PauseMsg(pausing), ActorRef.noSender());
                }
            }
        });

        this.stopButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                ActorSelection selection = context.actorSelection("/user/*");
                selection.tell(PoisonPill.getInstance(), ActorRef.noSender());
                update(ObserverType.RESET, 0);
            }

        });
        this.stopButton.setVisible(false);

    }

    @Override
    public void update(ObserverType observerType, Object value) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                switch (observerType) {
                    case TURN:
                        turn.setText(String.valueOf(value));
                        break;
                    case NUMBER:
                        oracleNumber.setText(String.valueOf(value));
                        startButton.setText("Pause");
                        winnerLabel.setText("");
                        stopButton.setVisible(true);
                        break;
                    case WINNER:
                        winnerLabel.setText("P" + String.valueOf(value) + " wins!");
                        startButton.setText("New Game");
                        isRunning = false;
                        stopButton.setVisible(false);
                        break;
                    case PLAY_PAUSE:
                        startButton.setText((boolean)value?"Continue":"Pause");
                        break;
                    case RESET:
                        oracleNumber.setText("?");
                        turn.setText("?");
                        winnerLabel.setText("");
                        startButton.setText("New game");
                        isRunning = false;
                        stopButton.setVisible(false);
                        break;
                }

            }
        });
}

}


