package ass03.ex1.messages;

import ass03.ex1.utils.Observer;

/**
 * Start Game Message.
 * From the View to the Oracle to begin.
 * From the Oracle to the TicketDispenser.
 * From the TicketDispenser to the Players.
 */
public class GameStartedMsg {
	
	private int numPlayers;
	private Observer observer;

	public GameStartedMsg(Observer observer){
		this.observer = observer;
	}

	public GameStartedMsg(int numPlayers, Observer observer) {
		this.numPlayers = numPlayers;
		this.observer = observer;
	}

	public int getNumPlayers() {
		return this.numPlayers;
	}

	public Observer getObserver(){
		return this.observer;
	}
}

