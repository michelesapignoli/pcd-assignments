package ass03.ex1.messages;


/**
 * Got Ticket Message.
 * From TicketDispenser to Players and from Players to the Oracle.
 *
 * @author Michele Sapignoli
 */
public class GotTicketMsg {
    private long query;

    public GotTicketMsg(){}

    public GotTicketMsg(long query) {
        this.query = query;
    }

    public long getQueryNumber() {
        return this.query;
    }
}
