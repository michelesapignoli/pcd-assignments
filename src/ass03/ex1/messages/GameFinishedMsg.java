package ass03.ex1.messages;

/**
 * Game finished message.
 * From winner (Player) to TicketDispenser ands TicketDispenser to Players.
 *
 * @author Michele Sapignoli
 */
public class GameFinishedMsg{}
