package ass03.ex1.messages;

/**
 * Result Message.
 * From Oracle to Player;
 *
 * @author Michele Sapignoli
 */
public class ResultMsg{
    private Result result;

    public ResultMsg(Result oracleResult){
        this.result = oracleResult;
    }

    public Result getResult(){
        return this.result;
    }
}
