package ass03.ex1.messages;

/**
 * Created by sapi9 on 15/05/2017.
 */
public class PauseMsg {
    private boolean pausing;

    public PauseMsg(boolean pausing){
        this.pausing = pausing;
    }
    public boolean isPausing(){
        return this.pausing;
    }
}
