package ass03.ex1.messages;

/**
 * Result enum.
 *
 * @author Michele Sapignoli
 */
public enum Result {
    GREATER,
    LESS,
    FOUND
}
