package ass03.ex1.utils;

/**
 * ObserverType enum.
 *
 * @author Michele Sapignoli
 */
public enum ObserverType{
    NUMBER,
    TURN,
    WINNER,
    RESET,
    PLAY_PAUSE
}
