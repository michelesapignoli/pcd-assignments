package ass03.ex1.utils;

import javax.swing.*;

/**
 * Observer class extending JFrame.
 *
 * @author Michele Sapignoli
 */

public abstract class Observer extends JFrame {
    /**
     * Method called by the actors to notify the view.
     * @param observerType
     * @param value
     */
    public abstract void update(ObserverType observerType, Object value);
}
