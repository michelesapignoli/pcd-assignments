package ass03.ex1.utils;

/**
 * Constants class.
 *
 * @author Michele Sapignoli
 */
public class Strings {
    public static final String ORACLE_ACTOR = "/user/Or";
    public static final String ORACLE_NAME = "Or";
    public static final String TD_ACTOR = "/user/Or/TD";
    public static final String TD_NAME = "TD";
    public static final String PLAYER_ACTOR = "/user/Or/TD/P";
    public static final String PLAYER_NAME = "P";
    public static final String SYSTEM_NAME = "GuessTheNumberActors";
    public static final int PLAYERS = 4;

}
