package ass03.ex1.actors;

import akka.actor.AbstractActor;
import ass03.ex1.utils.Observer;
import ass03.ex1.utils.ObserverType;

/**
 * Abstract Class actor.
 * Extends from Akka class, implements a method to notify the view of variables change.
 *
 * @author Michele Sapignoli
 */
public abstract class Actor extends AbstractActor {

    protected Observer observer;

    public void notifyObserver(ObserverType ot, Object value){
        this.observer.update(ot, value);

    }
}
