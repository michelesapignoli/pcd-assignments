package ass03.ex1.actors;

import java.util.concurrent.ThreadLocalRandom;

import ass03.ex1.utils.ObserverType;
import ass03.ex1.messages.*;
import ass03.ex1.messages.ResultMsg;
import ass03.ex1.utils.Strings;

/**
 * Player actor.
 *
 * @author Michele Sapignoli
 */
public class Player extends Actor {

    private int id;
    private long queryNumber;
    private long min = 0;
    private long max = Long.MAX_VALUE;

    /**
     * Player constructor. When it is firsly instantiated, it computes a random number.
     *
     * @param i player id
     */
    public Player(int i) {
        this.id = i;
        this.queryNumber = ThreadLocalRandom.current().nextLong(this.min, this.max);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(GameStartedMsg.class, msg -> {
                 /* Player can book a ticket from the Ticket Dispenser
                 */
                    this.observer = msg.getObserver();
                    getContext().sender().tell(new BookingMsg(), getSelf());
                })
                .match(GotTicketMsg.class, msg -> {
                 /* Ticket Dispenser gave the ticket, Player can ask the Oracle
                 */
                    System.out.println("Is it " + this.queryNumber + "? | Player " + this.id);
                    getContext().actorSelection(Strings.ORACLE_ACTOR)
                            .tell(new GotTicketMsg(this.queryNumber), getSelf());
                })
                .match(ResultMsg.class, msg -> {
                 /* Result given by the Oracle - Player either wins (stops) or computes a number again and books the ticket from the TD
                 */
                    switch (msg.getResult()) {
                        case FOUND:
                            System.out.println("Won! | Player " + this.id);
                            getContext().actorSelection(Strings.TD_ACTOR).tell(new GameFinishedMsg(), getSelf());
                            this.notifyObserver(ObserverType.WINNER, this.id);
                            this.getContext().stop(getSelf());
                            break;
                        case LESS:
                            this.max = this.queryNumber;
                            this.queryNumber = ThreadLocalRandom.current().nextLong(this.min, this.max);
                            getContext().actorSelection(Strings.TD_ACTOR).tell(new BookingMsg(), getSelf());
                            break;
                        case GREATER:
                            this.min = this.queryNumber;
                            this.queryNumber = ThreadLocalRandom.current().nextLong(this.min, this.max);
                            getContext().actorSelection(Strings.TD_ACTOR).tell(new BookingMsg(), getSelf());
                            break;

                    }
                })
                .match(GameFinishedMsg.class, msg -> {
                 /* Player loss
                 */
                    System.out.println("sob :( | Player " + this.id);
                    this.getContext().stop(getSelf());
                })
                .matchAny(o -> System.out.println("Unknown message"))
                .build();
    }
}
