package ass03.ex1.actors;

import java.util.concurrent.ThreadLocalRandom;

import akka.actor.ActorRef;
import akka.actor.Props;
import ass03.ex1.messages.*;
import ass03.ex1.messages.Result;
import ass03.ex1.utils.ObserverType;
import ass03.ex1.utils.Strings;

/**
 * Oracle actor.
 *
 * @author Michele Sapignoli
 */
public class Oracle extends Actor {

    private long oracleNumber;

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(GameStartedMsg.class, msg -> {

                 /* Game started: Oracle computes the secret number and notifies it to the View.
                 * Then tells the Ticket Dispenser tha game has started.
                 */
                    this.oracleNumber = ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE);
                    this.observer = msg.getObserver();
                    this.notifyObserver(ObserverType.NUMBER, this.oracleNumber);
                    System.out.println("(The secret number is... " + this.oracleNumber +") | Oracle");
                    ActorRef td = getContext().actorOf(Props.create(TicketDispenser.class), Strings.TD_NAME);
                    td.tell(new GameStartedMsg(Strings.PLAYERS, msg.getObserver()), ActorRef.noSender());
                })
                .match(GotTicketMsg.class, msg ->{
                    /*
                 * GotTicket msg: a player with a ticket has arrived to the Oracle for asking.
                 * The Oracle gives a result. If found, stops.
                 */
                    long queryNumber = msg.getQueryNumber();
                    Result result = this.getResult(queryNumber);
                    System.out.println(result + " | Oracle");
                    getContext().sender().tell(new ResultMsg(result), getSelf());
                    if(result == Result.FOUND){
                        this.getContext().stop(this.getSelf());
                    }
                })
                .matchAny(o -> System.out.println("Unknown message"))
                .build();
    }

    private Result getResult(long queryNumber){
        return this.oracleNumber == queryNumber ? Result.FOUND :
                (this.oracleNumber < queryNumber ? Result.LESS : Result.GREATER );
    }


}
