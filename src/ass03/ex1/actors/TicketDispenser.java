package ass03.ex1.actors;

import java.util.LinkedList;
import java.util.Queue;

import akka.actor.ActorRef;
import akka.actor.Props;
import ass03.ex1.utils.ObserverType;
import ass03.ex1.messages.*;
import ass03.ex1.utils.Strings;

/**
 * Ticket Dispenser actor.
 *
 * @author Michele Sapignoli
 */
public class TicketDispenser extends Actor {

    private int turn;
    private int counter;
    private int nPlayers;
    private boolean pausing;
    private Queue<ActorRef> queue;


    public void preStart() {
        this.queue = new LinkedList<>();
        this.pausing = false;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(GameStartedMsg.class, msg -> {
                 /* TicketDispenser tells the players the game has begun
                 */
                    this.nPlayers = msg.getNumPlayers();
                    this.observer = msg.getObserver();
                    for (int i = 0; i < nPlayers; i++) {
                        ActorRef player = getContext().actorOf(Props.create(Player.class, i), Strings.PLAYER_NAME + i);
                        player.tell(new GameStartedMsg(this.observer), getSelf());
                    }
                })
                .match(BookingMsg.class, msg -> {
                     /* A BookingMsg has arrived from one of the players: when the queue is full the TD gives a ticket to the first
                     */
                    if(getSender() != getSelf()){
                     this.queue.add(getSender());
                    }

                    if (!this.pausing) {
                        /*
                        *
                        * -------------------------- Sleep to slow down the game: remove for better performance
                        *
                        *
                         */
                        Thread.sleep(20);

                        if (queue.size() == this.nPlayers) {
                            this.counter++;
                            if (this.counter % this.nPlayers == 0) {
                                this.incrementTurn();
                            }
                            System.out.println("Next Player please! | TD");
                            this.queue.remove().tell(new GotTicketMsg(), getSelf());
                        }
                    }
                })
                .match(PauseMsg.class, msg -> {
                    this.pausing = msg.isPausing();
                    if(!msg.isPausing()){
                        getSelf().tell(new BookingMsg(), getSelf());
                    }
                    this.notifyObserver(ObserverType.PLAY_PAUSE, msg.isPausing());
                })
                .match(GameFinishedMsg.class, msg -> {
                 /* The TD tells the player the game has ended
                 */
                    for (ActorRef player : getContext().getChildren()) {
                        if (player != getSender()) {
                            player.tell(new GameFinishedMsg(), getSelf());
                        }
                    }
                    this.getContext().stop(this.getSelf());
                })
                .matchAny(o -> System.out.println("Unknown message"))
                .build();
    }

    private void incrementTurn() {
        this.turn++;
        this.notifyObserver(ObserverType.TURN, this.turn);
    }


}
