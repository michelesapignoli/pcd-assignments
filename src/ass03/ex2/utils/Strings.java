package ass03.ex2.utils;

/**
 * Constants class.
 *
 * @author Michele Sapignoli
 */
public class Strings {
	public static final String MSG_TYPE = "type";
	public static final String VALUE = "val";
}
