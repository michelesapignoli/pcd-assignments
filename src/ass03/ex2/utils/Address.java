package ass03.ex2.utils;

/**
 * Adresses class.
 * Strings needed as the publish and send method by Vertx require addresses.
 * 
 * @author Michele Sapignoli
 */
public class Address {
	public static final String PLAYER = "user.player";
	public static final String TICKET_DISPENSER = "user.ticketdispenser";
	public static final String ORACLE = "user.oracle";
	public static final String VIEW = "view";
}
