package ass03.ex2.utils;
/**
 * MessageType enum.
 * 
 * @author Michele Sapignoli
 */
public enum MessageType {
	
	BOOKING_MSG, //Published by Player, handled by Ticket Dispenser
	TICKET_MSG, //Published by TicketDispenser, handled by Player
	WIN_MSG, //Published by Player, handled by TicketDispenser
	GAMEOVER_MSG, //Published by Player, handled by Player
	QUERY_MSG, //Published by Player, handled by Oracle
	PAUSE_MSG, //Published by View, handled by TicketDispenser
	UPDATE_WINNER_MSG, //Published by TicketDispenser, handled by View
	UPDATE_ORACLE_NUMBER_MSG, //Published by Oracle, handled by View
	UPDATE_TURN_MSG, //Published by TicketDispenser, handled by View
	RESET, //Used to reset the View labels
	AUTO_MSG, //Published by TicketDispenser, handled by TicketDispenser
}
