package ass03.ex2.utils;
/**
 * Result enum.
 * 
 * @author Michele Sapignoli
 */
public enum Result {
	GREATER,
    LESS,
    FOUND
}
