package ass03.ex2.verticles;

import ass03.ex2.utils.Address;
import ass03.ex2.utils.MessageType;
import ass03.ex2.utils.Strings;
import io.vertx.core.eventbus.Message;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * View class.
 * Turned into Verticle, to send/receive message from the other Verticles.
 * It deploys the Oracle Verticle and send pausing messages to the Ticket Dispenser Verticle.
 * It receives messages from the Verticles to update the frame.
 *
 * @author Michele Sapignoli
 */
public class View extends Verticle {

	public boolean isRunning;
	public boolean pausing;

	public JButton startButton;
	public JButton stopButton;
	public JLabel numberLabel;
	public JLabel turnLabel;
	public JLabel turn;
	public JLabel oracleNumber;
	public JLabel winnerLabel;

	private Oracle o;


	@Override
	public void registerConsumers() {
		this.vertx.eventBus().consumer(Address.ORACLE, this::handler); 	
		this.vertx.eventBus().consumer(Address.TICKET_DISPENSER, this::handler); 			
	}

	@Override
	public void behaveStart() {
		this.isRunning = false;
		this.pausing = false;
		this.o = new Oracle();
		this.createFrame();

	}

	private void handler(Message<Object> message){
		this.update(MessageType.valueOf(message.headers().get(Strings.MSG_TYPE)), message.body());
	}

	private void createFrame() {
		JFrame frame = new JFrame("Verticles");
		frame.setLayout(new BorderLayout());
		frame.setSize(750, 150);
		JPanel panel = new JPanel();
		frame.add(panel);
		this.startButton = new JButton("New Game");
		this.stopButton = new JButton("Stop & exit");
		panel.add(this.startButton);
		panel.add(this.stopButton); 

		this.numberLabel = new JLabel("Oracle number: ");
		this.turnLabel = new JLabel("Turn: ");
		this.oracleNumber = new JLabel("?");
		this.turn = new JLabel("?");
		this.winnerLabel = new JLabel("");
		this.winnerLabel.setForeground(Color.red);

		panel.add(numberLabel);
		panel.add(oracleNumber);
		panel.add(turnLabel);
		panel.add(turn);
		panel.add(winnerLabel);

		this.startButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!isRunning){
					isRunning = true;
					vertx.deployVerticle(o);
					update(MessageType.PAUSE_MSG, pausing);
				} else{
					pausing = !pausing;
					update(MessageType.PAUSE_MSG, pausing);
					vertx.eventBus().publish(Address.VIEW, pausing, getDeliveryOptions(MessageType.PAUSE_MSG));
				}

			}
		});

		this.stopButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				vertx.close();
				System.exit(0);
			}

		});
		this.stopButton.setVisible(false);
		frame.setVisible(true);
	}

	public void update(MessageType messageType, Object value) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				switch (messageType) {
				case UPDATE_TURN_MSG:
					turn.setText(String.valueOf(value));
					break;
				case UPDATE_ORACLE_NUMBER_MSG:
					oracleNumber.setText(String.valueOf(value));
					startButton.setText("Pause");
					winnerLabel.setText("");
					turn.setText("?");
					winnerLabel.setText("");
					stopButton.setVisible(true);
					break;
				case UPDATE_WINNER_MSG:
					startButton.setText("New Game");
					isRunning = false;
					stopButton.setVisible(false);
					break;
				case PAUSE_MSG:
					startButton.setText((boolean)value?"Continue":"Pause");
					break;    
				case GAMEOVER_MSG:
					startButton.setText("New game");
					winnerLabel.setText("P" + String.valueOf(value) + " wins!");
					isRunning = false;
					stopButton.setVisible(false);
					break;
				case RESET:
					winnerLabel.setText("");
					turn.setText("?");
					oracleNumber.setText("?");
					startButton.setText("New game");
					isRunning = false;
					pausing = false;
					stopButton.setVisible(false);
					break;
				default:break;
				}

			}
		});
	}


}


