package ass03.ex2.verticles;

import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;

import java.util.LinkedList;
import java.util.Queue;

import ass03.ex2.utils.Address;
import ass03.ex2.utils.MessageType;
import ass03.ex2.utils.Strings;

/**
 * TicketDispenser Verticle.
 * Deployed by the Oracle.
 * Send messages on the EventBus that Player (TICKET_MSG, GAMEOVER_MSG) and View (UPDATE_TURN_MSG) handle.
 * Read messages from the EventBus (BOOKING_MSG, WIN_MSG) coming from the Player and (PAUSE_MSG) from the View.
 * 
 * 
 * @author Michele Sapignoli
 */
public class TicketDispenser extends Verticle {
	private MessageConsumer<Object> player;
	private MessageConsumer<Object> view;

	private int turn;
	private int counter;
	private int nPlayers = 4;
	private boolean pausing;
	private Queue<Integer> queue = new LinkedList<>();

	@Override
	public void registerConsumers() {
		this.player = this.vertx.eventBus().consumer(Address.PLAYER, this::handler);
		this.view = this.vertx.eventBus().consumer(Address.VIEW, this::handler);
	}

	@Override
	public void behaveStart(){
		for(int i = 0; i < this.nPlayers; i++) {
			this.vertx.deployVerticle(new Player(i));
		}   
	}

	private void handler(Message<Object> message){

		switch(MessageType.valueOf(message.headers().get(Strings.MSG_TYPE))){
		case BOOKING_MSG: this.gotBookingMsg(message);
			break;
		case WIN_MSG: this.gotTellGameIsFinishedMsg(message);
			break;
		case PAUSE_MSG: 
			this.gotPauseMsg(message);
			break;
		default:
			break;
		}
	}

	private void gotPauseMsg(Message<Object> message){
		this.pausing=(boolean)message.body();
		if(!this.pausing){

			this.gotBookingMsg(message);
		}

	}

	private void gotBookingMsg(Message<Object> message) {
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		try{
			this.queue.add((int)message.body());
		} catch(Exception e){
			System.out.println("Restarted | TD");
		}

		if(!this.pausing){
			if(this.queue.size() == this.nPlayers) {
				this.counter++;
				if (this.counter % this.nPlayers == 0) {
					this.incrementTurn();
				}
				System.out.println("Next Player please! | TD");

				this.vertx.eventBus().publish(Address.TICKET_DISPENSER, queue.remove(), this.getDeliveryOptions(MessageType.TICKET_MSG));
			}
		}

	} 

	private void gotTellGameIsFinishedMsg(Message<Object> message){
		this.vertx.eventBus().publish(Address.TICKET_DISPENSER, message.body(), this.getDeliveryOptions(MessageType.GAMEOVER_MSG));
		this.player.unregister();
		this.view.unregister();
	}

	private void incrementTurn() {
		this.turn++;
		this.vertx.eventBus().publish(Address.TICKET_DISPENSER, this.turn, this.getDeliveryOptions(MessageType.UPDATE_TURN_MSG));
	}



}
