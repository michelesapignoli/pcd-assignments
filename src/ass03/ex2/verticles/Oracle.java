package ass03.ex2.verticles;

import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.Json;
import java.util.concurrent.ThreadLocalRandom;

import ass03.ex2.utils.Address;
import ass03.ex2.utils.MessageType;
import ass03.ex2.utils.Result;
import ass03.ex2.utils.Strings;

/**
 * Oracle Verticle.
 * Deployed by the View.
 * Send messages on the EventBus that View (UPDATE_ORACLE_NUMBER_MSG) and Player (reply of QUERY_MSG) handle.
 * Read messages from the EventBus (BQUERY_MSG) coming from the Player.
 * 
 * 
 * @author Michele Sapignoli
 */
public class Oracle extends Verticle {

	private MessageConsumer<Object> player;
	private MessageConsumer<Object> view;

	private long oracleNumber = 0;


	@Override
	public void registerConsumers() {
		this.player = this.vertx.eventBus().consumer(Address.PLAYER, this::handler); 	
		this.view = this.vertx.eventBus().consumer(Address.VIEW, this::handler); 	
	}

	@Override
	public void behaveStart(){
		this.oracleNumber = ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE);
		System.out.println("(The secret number is... " + this.oracleNumber +") | Oracle");     
		this.vertx.eventBus().publish(Address.ORACLE, this.oracleNumber, this.getDeliveryOptions(MessageType.UPDATE_ORACLE_NUMBER_MSG));
		this.vertx.deployVerticle(new TicketDispenser());
	}

	private void handler(Message<Object> message){
		switch(MessageType.valueOf(message.headers().get(Strings.MSG_TYPE))){
		case QUERY_MSG: this.gotQueryMsg(message); 
		break;
		default:
			break;

		}
	}

	private void gotQueryMsg(Message<Object> message) {
		long queryNumber = (Long) message.body();

		Result result = this.oracleNumber == queryNumber ? Result.FOUND :
			(this.oracleNumber < queryNumber ? Result.LESS : Result.GREATER );
		System.out.println(result + " | Oracle");

		message.reply(Json.encode(result));

		if(queryNumber == this.oracleNumber){            
			this.view.unregister();
			this.player.unregister();
		}
	}
}
