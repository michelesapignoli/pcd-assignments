package ass03.ex2.verticles;

import ass03.ex2.utils.MessageType;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.DeliveryOptions;

/**
 * Abstract Class Verticle.
 * Extends from the Vertx class AbstractVerticle.
 * 
 * @author Michele Sapignoli
 */
public abstract class Verticle extends AbstractVerticle {

	/**
	 * Method start() calls abstract method registerConsumers() and behaveStart(),
	 * implemented in sub-classes.
	 *  
	 */
	@Override
	public void start(){
		this.registerConsumers();
		this.behaveStart();
	}

	public DeliveryOptions getDeliveryOptions(MessageType messageType){
		DeliveryOptions options = new DeliveryOptions();
		options.addHeader("type", String.valueOf(messageType));
		return options;
	}   

	/**
	 * Register consumers inside here.
	 */
	public abstract void registerConsumers();
	
	/**
	 * Method called by the start() method.
	 */
	public abstract void behaveStart();
}
