package ass03.ex2.verticles;

import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.Json;
import java.util.concurrent.ThreadLocalRandom;

import ass03.ex2.utils.Address;
import ass03.ex2.utils.MessageType;
import ass03.ex2.utils.Result;
import ass03.ex2.utils.Strings;

/**
 * Player Verticle.
 * Deployed by the Ticket Dispenser.
 * Send messages on the EventBus that Ticket Dispenser (BOOKING_MSG) and Oracle (QUERY_MSG) handle.
 * Read messages from the EventBus (TICKET_MSG, GAMEOVER_MSG) coming from TicketDispenser and the reply of QUERY_MSG from the Oracle.
 * 
 * 
 * @author Michele Sapignoli
 */
public class Player extends Verticle {

	private MessageConsumer<Object> td;

	private int id;
	private long queryNumber;
	private long min = 0;
	private long max = Long.MAX_VALUE;

	public Player (int id) {
		this.id = id;   
	}

	@Override
	public void registerConsumers() {
		this.td = this.vertx.eventBus().consumer(Address.TICKET_DISPENSER).handler(this::handler);
	}

	@Override
	public void behaveStart(){
		this.queryNumber = ThreadLocalRandom.current().nextLong(0, Long.MAX_VALUE);
		this.vertx.eventBus().publish(Address.PLAYER, this.id, this.getDeliveryOptions(MessageType.BOOKING_MSG));
	}

	private void handler(Message<Object> message){
		switch(MessageType.valueOf(message.headers().get(Strings.MSG_TYPE))){
		case TICKET_MSG: this.gotTicketMsg(message); 
		break;
		case GAMEOVER_MSG: this.gotGameFinishedMsg(message);
		break;
		default:
			break;

		} 	
	}

	private void gotTicketMsg(Message<Object> message) {

		int playerId = (int)message.body();
		if(playerId != this.id){
			return;
		}

		System.out.println("Is it " + this.queryNumber + "? | Player " + this.id);

		this.vertx.eventBus().send(Address.PLAYER, this.queryNumber, this.getDeliveryOptions(MessageType.QUERY_MSG), msg -> {

			Result result =  Json.decodeValue(String.valueOf(msg.result().body()), Result.class);

			switch(result){
			case FOUND:
				System.out.println("Won! | Player " + this.id);
				vertx.eventBus().send(Address.PLAYER, this.id, this.getDeliveryOptions(MessageType.WIN_MSG));
				this.td.unregister();
				break;
			case LESS:
				this.max = this.queryNumber;
				this.queryNumber = ThreadLocalRandom.current().nextLong(this.min, this.max);
				this.vertx.eventBus().send(Address.PLAYER, this.id, this.getDeliveryOptions(MessageType.BOOKING_MSG));
				break;
			case GREATER:
				this.min = this.queryNumber;
				this.queryNumber = ThreadLocalRandom.current().nextLong(this.min, this.max);
				this.vertx.eventBus().send(Address.PLAYER, this.id, this.getDeliveryOptions(MessageType.BOOKING_MSG));
				break;
			}         
		});
	}

	private void gotGameFinishedMsg(Message<Object> message) {
		System.out.println("sob :( | Player " + this.id);
		this.td.unregister();
	}
}
