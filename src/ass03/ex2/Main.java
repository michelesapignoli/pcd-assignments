package ass03.ex2;
import ass03.ex2.verticles.View;
import io.vertx.core.Vertx;

/**
 * Main class.
 * Deploys the View Verticle.
 * 
 * @author Michele Sapignoli
 */
public class Main {


	public static void main(String[] args) throws Exception {
		Vertx.vertx().deployVerticle(new View());
	}


}